import express from "express";
import * as yup from 'yup';
import {v4 as uuidv4 } from 'uuid';
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { next } from "sucrase/dist/parser/tokenizer";
const app = express();
app.use(express.json());

const configJwt = {
    secret: 'qualquer_coisa',
    expiresIn: '1h'
}

app.use(express.json())

const accounts = []

const mySchema = yup.object().shape({
    username:  yup.string().required(),
    age: yup.number().required().positive().integer(),
    email: yup.string().email().required(), 
    password: yup.string().required(),
    createdOn: yup.date().default(function () {
        return new Date()
    }),
    uuid: yup.string().default(() => {return uuidv4()}).transform((value, originalValue) => {
        return uuidv4()  
    })
  });

const validateData = (schema) => async (req, res, next) => {
    const body = req.body
    try {
    const validatedData = await schema.validate(body);

        req.body = validatedData;
    } catch (e) {
        return res.status(400).json(e.errors);
    }
    next()

    }

const authorization = (req, res, next) => {
    if(req.user.uuid != req.params.uuid) {
        return res.status(401).json({ message: 'You can only change your own password' })
    }
    next()
}

const isAuthenticated = (req, res, next) => {
    if (req.headers.authorization === undefined) {
        return res.status(401).json({ message: 'Missing authorization header' });
    }


    const token = req.headers.authorization.split(' ')[1]

    try {
        const decoded = jwt.verify(token, configJwt.secret)
        const user = accounts.find(user => user.username === decoded.username);
        req.user = user;
        next()
    }  catch (e) {
        return res.status(403).json({ message: 'Invalid token'});
    }

}

app.use('/signup', validateData(mySchema))



app.post('/signup', (req, res) => {
    const { password, username, age, email, uuid, createdOn } = req.body;
    const passwordHashed = bcrypt.hashSync(password, 10);

    const user = {
        ...req.body,
        password: passwordHashed
    };

    const user2 = {
        "uuid": uuid,
        "createdOn": createdOn,
        "email": email,
        "age": age,
        "username": username
    }  

    accounts.push(user);
    res.status(201).json(user2);
});

app.get('/users', isAuthenticated, (req, res) => {
    res.send(accounts);
});



app.post('/login', (req, res) => {
    const {username, password} = req.body
    const user = accounts.find(user => user.username === username)

    if(!user) {
        res.status(401).json({'message': 'User not found'})
    }

    if (!bcrypt.compareSync(password, user.password)) {
        return res.status(401).json({ message: 'Invalid credentials!' });
    }
    const token = jwt.sign({ username }, configJwt.secret, { expiresIn: configJwt.expiresIn });

    res.json({ token })


})


app.put('/users/:uuid/password', isAuthenticated, authorization, (req, res) => {
    const { password } = req.body;
    const { uuid } = req.params
    const passwordHashed = bcrypt.hashSync(password, 10);
    

    const accountsUiid = accounts.findIndex(account => account.uuid == uuid)


    accounts[accountsUiid] = {...accounts[accountsUiid], password: passwordHashed}

    res.status(204).send()
})


app.listen(3000, () => {
    console.log('Running at http://localhost:3000');
});


